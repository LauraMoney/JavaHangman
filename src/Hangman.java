import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class Hangman {
    public static void main(String[] args) throws Exception {
        
        //imports word list//
        File myWords = new File("C:/TEMP/words.txt");

        Scanner txtScanner = new Scanner(myWords);
        Scanner input = new Scanner(System.in);

        //adds words to array//
        List<String> words = new ArrayList<>();
            while (txtScanner.hasNextLine()) {
            words.add(txtScanner.nextLine());
        }

        //randomize words//
        Random rand = new Random();

        String word = words.get(rand.nextInt(words.size()));
        
        //test random words
        //System.out.println(word);
            
        List<Character> playerGuesses = new ArrayList<>();

        
        int wrongCount =0;
        while(true) {
        
        printHangMan(wrongCount);

        if(wrongCount >= 6) {
        System.out.println("LOSER!");
        System.out.println("The word was: " + word);
        break; 
        }

        printWordState(word, playerGuesses);
        if (!getPlayerGuess(input, word, playerGuesses)) {
            wrongCount++;
        }

        if(printWordState(word, playerGuesses)) {
            System.out.println("WINNER!");
            break;
        }

        System.out.println("PLEASE ENTER A GUESS:");
        if(input.nextLine().equals(word)) {
            System.out.println("WINNER!");
            break;
        }
        else {
            System.out.println("INCORRECT");
        }
    } 
}






    private static void printHangMan(int wrongCount) {
        System.out.println(" -------");
        System.out.println(" |     |");
        if (wrongCount >= 1) {
            System.out.println(" O");
        }

        if (wrongCount >= 2){
        System.out.print("\\ ");
        }

        if (wrongCount >= 3){
            System.out.println("/");
        }

        if (wrongCount >= 4) {
            System.out.println(" |");
        }
    
        if (wrongCount >= 5) {
            System.out.print("/ ");
            if (wrongCount >= 6) {
              System.out.println("\\");
            }
            else {
              System.out.println("");
            }
          }
          System.out.println("");
          System.out.println("");
    }

    
    
    


    private static boolean getPlayerGuess(Scanner input, String word, List<Character> playerGuesses) {
        System.out.println("PLEASE ENTER A LETTER:");
        String letterGuess = input.nextLine();
        playerGuesses.add(letterGuess.charAt(0));

        return word.contains(letterGuess);

        
    }

    private static boolean printWordState(String word, List<Character> playerGuesses) {
        int correctCount = 0;
        for (int i =0; i < word.length(); i++) {
            if(playerGuesses.contains(word.charAt(i))){
                System.out.print(word.charAt(i));
                correctCount++;
            }
            else {
                System.out.print("-");
            }
        }
        System.out.println();

        return (word.length() == correctCount);
    }

}
